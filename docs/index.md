# 1. Project Title: Automatic door handle sanitizer

Door handles are one of the most common harbourers for harmful bacteria, causing infections .
That’s why we are going to make a door handle sanitizer. This project is connected to medical electronics. 
Not only it helps to control the spread of corona virus but also protects you and your family from getting infected. 
The device is an automated door handle sanitizer. It sprays a mixture of Dettol and water onto the handle of the door after every 30 minutes which will kill all the germs like covid-19. 
It can be used everywhere like home, hospitals, hotels, schools, at work and so on and it is not so expsensive and our product cannot be easily vandalized.  

## Overview & Features


1. **Servo motor:** The motion of the servo motor is used to press the spray top at a given time.

2. **Spray bottle:** Contains the solution that kills germs, bacteria and viruses.

3. **Arduino Uno:** is an open-source microcontroller board based on the Microchip ATmega328P microcontroller and developed by Arduino.cc. 

The board is equipped with sets of digital and analog input/output pins that may be interfaced to various expansion boards and other circuits.

4. **batteries:** 8 volts batteries are used to charge this set up.

5. **Anti-vandalizable box:** this particular box is not easy to open, which prevents componets from being stolen			  >

## Demo / Proof of work


*Demo:*

## PRODUCT PITCH 

<iframe width="560" height="315" src="https://www.youtube.com/embed/rcIWnwEiKBg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

1. https://www.youtube.com/watch?v=XXQxJ0WR6IQ

2. https://www.youtube.com/watch?v=UwrAAjS0U8s

*Team Green Legacy with the final product*

<img src="img/the group.jpeg" width="300" height="300" border="0" alt="" />

*The initial components*

<img src="img/the compents we got.jpeg" width="320" height="480" border="0" alt="" />


## Deliverables


*Project poster*

<img src="img/poster.jpeg" width="680" height="280" border="0" alt="" />


*Proof of concept working:*

1. https://www.youtube.com/watch?v=XXQxJ0WR6IQ

2. https://www.youtube.com/watch?v=UwrAAjS0U8s



*Product pitch deck:*

https://docs.google.com/presentation/d/19peqShw6DaEqRxjdys4OhU8NQVcqkVZg5cK0CNoFu0U/edit#slide=id.gab1e7e8466_0_799





## the team & contact


1. **Aditya Chigharoe** Project Mnager

2. **Karan Orie** Head of Business & Marketing

3. **Morgan Sodinomo** Technical manage

4. **Santosh Koelfat** Software and co- Technical manage

5. **Ravinesh Pirthipal** Technician

6. **Rainesh Rewat** Technical manager

6. **Rainesh Rewat** Technical manager




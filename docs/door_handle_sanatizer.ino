
#include "LowPower.h"
#include <Servo.h>
Servo myservo;  // create servo object to control a servo
int sleepCounter ;


void setup() 
{
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
}

void loop() 
{

// 0.5hours = 30min x 60sec = 1800sec
// 1800s / 8s =255

for ( sleepCounter = 255 ; sleepCounter>0 ; sleepCounter--)
{
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);  

}
  
  myservo.write(0);                  // sets the servo position according to the scaled value
  delay(1000);                        // waits for the servo to get there
  myservo.write(60);                  // sets the servo position according to the scaled value
  delay(1000);   
}

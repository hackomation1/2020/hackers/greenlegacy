# 3. Business & Marketing

The price of an automatic doorhandle sanitizer in the market is $120,-. It takes us $60,- to make our product. We are Opting to sell our product for 
$85,- to make it affordable for the customer. Our product is safe to use in 
public places, because our product cannot be easily vandalized.


## 3.1 Objectives

1. Install the automatic door handle sanitizer at some places like cafes, hotels, hospitals and see how it works.

2. Mass production of the automatic door handle sanitizer if the results are positive and if people are interested



## 3.2 Ananlyze your market & segments


1. **customer segments** It can be used in hospitals, cafes, hotels, schools, public places, at work and so on.
2. **value proposition** The customer gets a good and high quality “Automatic Door Handle Sanitizer” made in Suriname with the maximum of local available materials at an affordable price.
3. **monetizable pain** the price of an automatic doorhandle sanitizer in the market is $120,- . We opt to sell our product for #85,-.
 

## 3.3 Build your pitch deck

*Pitch Deck*: 
https://docs.google.com/presentation/d/19peqShw6DaEqRxjdys4OhU8NQVcqkVZg5cK0CNoFu0U/edit#slide=id.gab1e7e8466_0_799>

## 3.4 Make a project Poster

<img src="img/poster.jpeg" width="1280" height="841" border="0" alt="" />



## 3.5 Files & Code

**Automatic doorhandle sanitizer code**

#include "LowPower.h"

#include <Servo.h>

Servo myservo;  // create servo object to control a servo
int sleepCounter ;


void setup() 
{
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
}

void loop() 
{

// 0.5hours = 30min x 60sec = 1800sec
// 1800s / 8s =255

for ( sleepCounter = 255 ; sleepCounter>0 ; sleepCounter--)
{
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);  

}
  
  myservo.write(0);                  // sets the servo position according to the scaled value
  delay(1000);                        // waits for the servo to get there
  myservo.write(60);                  // sets the servo position according to the scaled value
  delay(1000);   
}

## 3.6 References & Credits


Aatik's Lab (2020). Automatic doorhandle sanitizer (https://drive.google.com/drive/folders/1H2PROfy4TTdjJzXVa9-_guyBNYOsGyB4), 
MATLAB Central File Exchange. Retrieved October 14, 2020.

**Libraries**
Lim Phang Moh (2018). LowPower Library (https://github.com/rocketscream/Low-Power/commit/9f54bde6ebabf7bc48c15ee4985f70271575e49f), 
ROcketscream/LowPower. Retrieved October 16, 2020.

Copyright (c) 2013 Arduino LLC. All right reserved. Copyright (c) 2009 Michael Margolis. All right reserved. Servo Library (https://github.com/arduino-libraries/Servo), 
ROcketscream/LowPower. Retrieved October 16, 2020.

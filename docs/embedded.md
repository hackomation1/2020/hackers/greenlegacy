# 2. Embedded solution

We are going to make an automatic door handle sanitizer.  
It not only helps us to control the spread of Coronavirus and other bacterias but also protects everybody from getting infected.

## 2.1 Objectives
1. Install the automatic door handle sanitizer at some places like cafes, hotels, hospitals and see how it works.

2. Mass production of the automatic door handle sanitizer if the results are positive and if people are interested


## 2.3 Steps taken
*The initial components*
<img src="img/the compents we got.jpeg" width="320" height="480" border="0" alt="" />

*working on the code*
<img src="img/technicals working on the code.jpeg" width="400" height="300" border="0" alt="" />

*Working on tinkercad*
<img src="img/tinkercad.jpeg" width="1065" height="435" border="0" alt="" />

*constructing a "so-called" door for the automatic door handle sanitizer*
<img src="img/door construction team.jpeg" width="400" height="300" border="0" alt="" />

*intalling the components in the anti-vandalizable box*
<img src="img/fp.jpg" width="300" height="300" border="0" alt="" />

*installing the automatic doorhandle sanitizer onto the "so-called" door*
<img src="img/installation proces.jpeg" width="300" height="400" border="0" alt="" />


## 2.4 Testing & Problems


**Problem**: We did not posses some essential libraries(LowPower.h, Servo.h & Lowpower.PowerDown) while coding.

*Solution*: We downloaded those libraries from the internet(see 2.7 references & credits)

**Problem**: Unable to understand the code we got from the Internet**(Aatik's Lab (2020))**.

*Solution*: We used the "Trial and Error" method to understand the code.


## 2.5 Proof of work

*Demo videos*
1. https://www.youtube.com/watch?v=XXQxJ0WR6IQ
2. https://www.youtube.com/watch?v=UwrAAjS0U8s>

## 2.6 Files & Code

**Automatic doorhandle sanitizer code**

#include "LowPower.h"

#include <Servo.h>

Servo myservo;  // create servo object to control a servo
int sleepCounter ;


void setup() 
{
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
}

void loop() 
{

// 0.5hours = 30min x 60sec = 1800sec
// 1800s / 8s =255

for ( sleepCounter = 255 ; sleepCounter>0 ; sleepCounter--)
{
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);  

}
  
  myservo.write(0);                  // sets the servo position according to the scaled value
  delay(1000);                        // waits for the servo to get there
  myservo.write(60);                  // sets the servo position according to the scaled value
  delay(1000);   
}

## 2.7 References & Credits
Aatik's Lab (2020). Automatic doorhandle sanitizer (https://drive.google.com/drive/folders/1H2PROfy4TTdjJzXVa9-_guyBNYOsGyB4), 
MATLAB Central File Exchange. Retrieved October 14, 2020.

**Libraries**
Lim Phang Moh (2018). LowPower Library (https://github.com/rocketscream/Low-Power/commit/9f54bde6ebabf7bc48c15ee4985f70271575e49f), 
ROcketscream/LowPower. Retrieved October 16, 2020.

Copyright (c) 2013 Arduino LLC. All right reserved. Copyright (c) 2009 Michael Margolis. All right reserved. Servo Library (https://github.com/arduino-libraries/Servo), 
ROcketscream/LowPower. Retrieved October 16, 2020.



>